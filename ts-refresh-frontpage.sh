#!/bin/bash

# This script regenerates the dynamic portion of the web site front page,
# stored in /mit/tech-squares/www/auto/index.shtml
# This script is run automatically at 1am every night.
# Note that scripts.mit.edu (which runs the nightly cron job) must have
# write permissions to www/auto -- so don't put anything else in there!
# Likewise, scripts.mit.edu must have read/execute permissions on
# this script and the java .jar files which contain the 'real' meat of
# the code, which is why they all live here in /mit/tech-squares/cron_scripts
# (and not in, say, /mit/tech-squares/arch/common/bin with all the other
# scripts).
# FINALLY: scripts.mit.edu runs with ulimit restrictions on stack and heap
# size, which causes java to die immediately with "Error occurred during
# initialization of VM".  We manually specify a heap size (-Xmx100M) to
# avoid this problem, but future tweaking may be required.
#
# More information on authoring events for the calendar can be found in
# /mit/tech-squares/cron_scripts/README
#                      C. Scott Ananian, cananian@alum.mit.edu, 17 June 2006

# this path to java might need to change as java evolves; sadly the
# "automagic" athena java-version-selection scripts don't work on
# scripts.mit.edu (according to SIPB)
JAVA=/mit/java/java_v1.6.0/bin/java

# special hack for scripts.mit.edu, which is an amd64 machine.
if [ "x86_64" = $(uname -m) ]; then
    JAVA=/mit/tech-squares/cron_scripts/jre1.6.0b2-for-scripts/bin/java
fi

if [ -x /usr/athena/bin/athrun ]; then
    JAVA="athrun java java"
fi
# another hack: as of 01-sep-2008, scripts.mit.edu started leaving the
# timezone unset.  Force it.
export TZ=US/Eastern

# die unless /mit/tech-squares/www/auto exists (which would probably
# mean that we'd reverted to our old web site, or some such)
if [ ! -d /mit/tech-squares/www/auto ]; then
    exit 0
fi

cd /mit/tech-squares/cron_scripts/
./gcal.py >| /mit/tech-squares/www/auto/tmp
# note that we're careful to only replace the existing front-page file
# if our java program executed without failure.
if [ $? -eq 0 ]; then
    # install the new version
    mv /mit/tech-squares/www/auto/tmp /mit/tech-squares/www/auto/index.shtml
else
    # clean up our mess
    rm /mit/tech-squares/www/auto/tmp
fi
