#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import httplib2
from datetime import datetime, timedelta
import pytz
import dateutil.parser
from oauth2client.client import SignedJwtAssertionCredentials
from apiclient import discovery
from collections import OrderedDict

CLIENT_EMAIL = '77070087000-n95pjgrggqr779mkd3f1c31g1sl1jobo@developer.gserviceaccount.com'
PRIVATE_KEY_FN = 'Tech Squares-18acebfa7133.p12'
MAIN_ID = 'tech.squares@gmail.com'
ANNOUNCEMENTS_ID = '66db7oa9fipt6driukqkdblrgk@group.calendar.google.com'

def get_creds():
  with open(PRIVATE_KEY_FN) as f:
    private_key = f.read()
  credentials = SignedJwtAssertionCredentials(CLIENT_EMAIL, private_key,
    'https://www.googleapis.com/auth/calendar.readonly')
  return credentials

def make_service():
  credentials = get_creds()
  auth_http = credentials.authorize(httplib2.Http())
  service = discovery.build('calendar', 'v3', http=auth_http)
  return service

def get_events_from_calendar(service, calendarId, daysBeforeToday, daysAfterToday):
  allEvents = []
  today = datetime.now(tz=pytz.timezone('US/Eastern')).replace(hour=0,minute=0,second=0,microsecond=0)
  timeMin = today - timedelta(days=daysBeforeToday)
  timeMax = today + timedelta(days=daysAfterToday)
  pageToken = None
  while True:
    events = service.events().list(
      calendarId=calendarId,
      timeMin=timeMin.isoformat(),
      timeMax=timeMax.isoformat(),
      orderBy="startTime",
      singleEvents=True,
      pageToken=pageToken).execute()
    try:
      for event in events['items']:
        allEvents.append(event)
    except KeyError:
      # events object has no items; ignore
      pass
    pageToken = events.get('nextPageToken')
    if not pageToken:
      break
  return allEvents

def get_events_by_date(service, calendarId, daysBeforeToday, daysAfterToday):
  events = get_events_from_calendar(service, calendarId, daysBeforeToday, daysAfterToday)
  eventsByDate = OrderedDict()
  for event in events:
    try:
      start = event['start']
    except KeyError:
      # event has no start time? this is weird and shouldn't happen
      # we can't do anything, so continue to next event
      continue
    try:
      date = dateutil.parser.parse(start['dateTime']).date()
    except KeyError:
      # event start time has no dateTime element; it should have a date
      try:
        date = dateutil.parser.parse(start['date']).date()
      except KeyError:
        # event start time has no date _or_ dateimte? This is dumb.
        # ignore event, continue with next one
        continue
    if date in eventsByDate:
      eventsByDate[date].append(event)
    else:
      eventsByDate[date] = [event]
  return eventsByDate

def generate_id(event):
  return 'id'

def generate_time(event):
  try:
    start = event['start']
  except KeyError:
    print "Event has no start time?"
  try:
    startDateTime = start['dateTime']
  except KeyError:
    # event has date but no time
    # return no time
    return ""
  return dateutil.parser.parse(startDateTime).time()

def generate_description(event):
  description = ''
  try:
    description += event['summary']
  except KeyError:
    # event has no summary? I don't think this should happen
    pass
  try:
    location = event['location']
    description += " -- %s" % location
  except KeyError:
    # no location, whatever
    pass
  return description

def generate_whats_happening_section(service):
  CALENDAR_LINK_URL = "http://www.google.com/calendar/render?cid=http%3A%2F%2Fwww.google.com%2Fcalendar%2Ffeeds%2Ftech.squares%40gmail.com%2Fpublic%2Fbasic"
  content = """\
<h1>What's happening at Tech Squares</h1>
<a class="rightalign" href="%(url)s"><img src="http://www.google.com/calendar/images/ext/gc_button1.gif" class="noborder" alt="[Google Calendar]" title="Add upcoming Tech Squares dances to your Google Calendar"/></a>

<dl class="events">
""" % {'url': CALENDAR_LINK_URL}
  eventsByDate = get_events_by_date(service, MAIN_ID, 0, 14)
  for date, events in eventsByDate.items():
    content += """<dt>%(date)s</dt>\n<dd>\n<ul>\n""" % {'date': date}
    for event in events:
      content += """<li><a id="%(id)s" class="time">%(time)s</a><span class="nocss"> </span>%(description)s</li>\n""" \
        % {'id': generate_id(event), 'time': generate_time(event), 'description': generate_description(event)}
    content += """</ul>\n</dd>\n"""
  content += """</dl>\n"""
  return content

def generate_announcements_section(service):
  content = ""
  announcements = get_events_from_calendar(service, ANNOUNCEMENTS_ID, 0, 1)
  for announcement in reversed(announcements):
    content += """<h2>%(title)s</h2>\n%(description)s\n\n""" % {'title': announcement['summary'], 'description': announcement['description']}
  return content

def main():
  service = make_service()
  content = ""
  content += generate_whats_happening_section(service)
  content += generate_announcements_section(service)
  print content

if __name__ == '__main__':
  main()
