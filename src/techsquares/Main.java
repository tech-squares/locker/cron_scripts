package techsquares;

import com.google.gdata.client.*;
import com.google.gdata.client.calendar.*;
import com.google.gdata.data.*;
import com.google.gdata.data.extensions.*;
import com.google.gdata.util.*;

import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.regex.Pattern;

/**
 * Quick-and-dirty code to generate Tech Squares index.html page based on
 * contents of Google Calendar feed.  There are two feeds involved: the
 * public dance feed generates the "What's happening at Tech Squares"
 * two-week view; the private announcements feed generates the rest.
 * This code could be a lot cleaner, sorry.
 * @author C. Scott Ananian <cananian@alumni.princeton.edu>
 */
public class Main {
  private static boolean DEBUG = false; // generate debug info
  private static boolean FULL=false; // generate a page header & footer

  public static void main(String[] args) throws java.io.IOException, ServiceException {
    CalendarService myService = new CalendarService("techsquares-calApp-1");
    
    // Send the request and receive the response:
  
    // okay, dump the xml
    if (FULL) printHeader();

    EventFeed agendaFeed = getFeed(myService, "http://www.google.com/calendar/feeds/tech.squares@gmail.com/public/full", 0, 14);
    if (DEBUG) dump(agendaFeed);
    printAgenda(myService, agendaFeed);

    myService = new CalendarService("techsquares-calApp-1");//XXX NOT SURE WHY THIS IS NECESSARY XXX
    // this feed url comes from the 'xml' button beside 'private address'
    // when you click on 'calendar settings' in google.com; the 'basic' at the
    // end needs to be replaced with 'full'.
    EventFeed announceFeed = getFeed(myService, "http://www.google.com/calendar/feeds/66db7oa9fipt6driukqkdblrgk@group.calendar.google.com/private-0de5f8641ae75edb45dc9200594831cf/full", 0, 1);
    if (DEBUG) dump(announceFeed);
    printAnnouncements(myService, announceFeed);

    if (FULL) printFooter();

    System.exit(0);
  }
  static EventFeed getFeed(CalendarService myService, String urlStr,
			   int daysBefore/*inclusive*/,
			   int daysAfter/*exclusive*/)
    throws java.io.IOException, ServiceException {
    URL url = new URL(urlStr);
    // filter: get events for next 13 days
    CalendarQuery myQuery = new CalendarQuery(url);
    Calendar now = Calendar.getInstance();
    // move to 'start of today' -- ie, 12:01am.
    now.set(Calendar.MILLISECOND, 0);
    now.set(Calendar.SECOND, 0);
    now.set(Calendar.MINUTE, 0);
    now.set(Calendar.HOUR_OF_DAY, 0);
    // ok, now move the appropriate number of days 'before'
    now.add(Calendar.DAY_OF_YEAR, -daysBefore);
    DateTime start = new DateTime(now.getTime(), now.getTimeZone());
    // and the number of days 'after'
    now.add(Calendar.DAY_OF_YEAR, daysBefore+daysAfter);
    DateTime end = new DateTime(now.getTime(), now.getTimeZone());
    // ok, construct the query.
    myQuery.setMinimumStartTime(start);
    myQuery.setMaximumStartTime(end);
    if (DEBUG) System.out.println(myQuery.getUrl());

    return myService.query(myQuery, EventFeed.class);
  }

  static void printHeader() {
    System.out.println("<!--#include virtual=\"header.html\" -->");
    System.out.println("<div id=\"main\">");
    System.out.println();
  }
  static void printAgenda(CalendarService svc, EventFeed ef) {
    System.out.println("<h1>What's happening at Tech Squares</h1>");
    System.out.println("<a class=\"rightalign\" href=\"http://www.google.com/calendar/render?cid=http%3A%2F%2Fwww.google.com%2Fcalendar%2Ffeeds%2Ftech.squares%40gmail.com%2Fpublic%2Fbasic\"><img src=\"http://www.google.com/calendar/images/ext/gc_button1.gif\" class=\"noborder\" alt=\"[Google Calendar]\" title=\"Add upcoming Tech Squares dances to your Google Calendar\"/></a>");
    System.out.println();
    System.out.println("<dl class=\"events\">");
    String heading = null;
    for (WhenEvent we : filter(ef.getEntries())) {
      DateTime dt = we.when.getStartTime();
      String dateString = dt2uidate(dt);
      if (heading==null || !heading.equals(dateString)) {
	if (heading!=null) {
	  System.out.println("</ul>");
	  System.out.println("</dd>");
	  System.out.println();
	}
	heading = dateString;
	String additionalInfo = null;
	System.out.print("<dt>"+heading);
	if (additionalInfo!=null)
	  System.out.print(": "+additionalInfo);
	System.out.println("</dt>");
	System.out.println("<dd>");
	System.out.println("<ul>");
      }
      // okay, deal with the individual event.
      System.out.print("<li>");
      System.out.print("<a id=\""+entry2id(we)+"\" class=\"time\">");
      if (!dt.isDateOnly())
	System.out.print(dt2uitime(dt)+":");
      System.out.print("</a>");
      System.out.print("<span class=\"nocss\"> </span>");//space for lynx
      System.out.print(tc2xhtml(we.event.getTitle()));
      String ws = where2ui(we.event.getLocations());
      if (ws!=null && ! "".equals(ws))
	  System.out.print(" in "+ws);
      // print any comments
      for (MessageEntry me : getComments(svc, we.event)) {
	System.out.println("<br />");
	System.out.print(cont2xhtml(me.getContent()));
      }
      System.out.println("</li>");
    }
    // clean up at the bottom.
    if (heading!=null) {
	  System.out.println("</ul>");
	  System.out.println("</dd>");
    }
    System.out.println("</dl>");
  }

  static List<MessageEntry> getComments(CalendarService svc, EventEntry ee) {
    Comments c = ee.getExtension(Comments.class);
    if (c==null) return Collections.emptyList();
    FeedLink fl = c.getFeedLink();
    if (fl==null) return Collections.emptyList();
    try {
      URL url = new URL(fl.getHref());
      MessageFeed mf = svc.getFeed(url, MessageFeed.class);
      return mf.getEntries();
    } catch (ServiceException se) {
      System.err.println(se);
      return Collections.emptyList();
    } catch (java.io.IOException e) {
      System.err.println(e);
      return Collections.emptyList();
    }
  }

  static void printAnnouncements(CalendarService svc, EventFeed ef) {
    // xxx other sort criteria?
    List<WhenEvent> l = new ArrayList<WhenEvent>(filter(ef.getEntries()));
    Collections.reverse(l); // sort announcements so newest is first.
    for (WhenEvent we : l) {
      System.out.print("<h2>");
      System.out.print(tc2xhtml(we.event.getTitle()));
      System.out.println("</h2>");
      // now print contents.
      System.out.println(cont2xhtml(we.event.getContent()));
      System.out.println();
      if (DEBUG) {
	// test comments
	for (MessageEntry me : getComments(svc, we.event))
	  System.out.println("<!-- "+cont2xhtml(me.getContent())+" -->");
      }
    }
  }
  static void printFooter() {
    System.out.println();
    System.out.println("</div>");
    System.out.println("<!--#include virtual=\"footer.html\" -->");
  }

  static String where2ui(List<Where> wheres) {
    List<String> l = new ArrayList<String>(wheres.size());
    for (Where w : wheres) {
	String s = where2ui(w);
	if (! "".equals(s.trim()))
	    l.add(s);
    }
    return list2ui(l, ""/*"location TBA"*/);
  }
  /** Take a list of strings and pretty-print them in human-readable form
   *  by separating with commas and 'and'. */
  static String list2ui(List<String> l, String emptyString) {
    // emit as "a, b, c, and d"
    switch(l.size()) {
    case 0:
      return emptyString;
    case 1:
      return l.get(0);
    case 2:
      return l.get(0)+" and "+l.get(1);
    default:
      String last = l.remove(l.size()-1);
      StringBuffer sb = new StringBuffer();
      for (String s : l) {
	sb.append(s);
	sb.append(", ");
      }
      sb.append("and ");
      sb.append(last);
      return sb.toString();
    }
  }
  static String where2ui(Where w) {
    // note that eventually google may support more semantic info here.
    String desc = w.getValueString();
    if (desc == null) return "";
    // special places we know about.
    for (Room r : rooms)
      if (r.matches(desc))
	return r.desc;
    // okay, just spit out the string on the calendar
    return htmlEscape(desc);
  }
  private static class Room {
    private final Pattern room;
    public final String desc;
    Room(Pattern room, String desc) {
      this.room = room; this.desc = desc;
    }
    Room(String name, String desc) {
      this(Pattern.compile("\\b" + name + "\\b", Pattern.CASE_INSENSITIVE),
	   desc);
    }
    Room(String name, String fullname, String id) {
      this(name, "<a href=\"directions/where-at-mit.html#"+id+"\">"+htmlEscape(fullname)+"</a>");
    }
    public boolean matches(String s) {
      return room.matcher(s).find();
    }
  }
  private static final Room[] rooms = {
    new Room("Sala", "Sala", "student-center"),
    new Room("Lobdell", "Lobdell", "student-center"),
    new Room("407", "W20-407", "student-center"),
    new Room("491", "W20-491", "student-center"),
    new Room("RRA", "Kresge Rehearsal Room A", "kresge"),
    new Room("RRB", "Kresge Rehearsal Room B", "kresge"),
    new Room("Kresge Rehearsal Room A", "Kresge Rehearsal Room A", "kresge"),
    new Room("Kresge Rehearsal Room B", "Kresge Rehearsal Room B", "kresge"),
    new Room("Walker", "Walker", "walker-memorial"),
    new Room("Morss", "Morss", "walker-memorial"),
    new Room("Lobby\\s+13", "Lobby 13", "lobby-13"),
    new Room("Burton", "Burton Dining Hall", "burton-dining-hall"),
    new Room("W92", "Building W92", "w92"),
  };

  static String dt2uidate(DateTime dt) {
    Date d = new Date(dt.getValue());
    DateFormat df = dateF;
    if (dt.isDateOnly()) {
      // time is 00:00 GMT; format in GMT or else we'll get the wrong date
      // note that dt.getTzShift() is 'null' for this value.
      df = (DateFormat) df.clone();
      df.setTimeZone(TimeZone.getTimeZone("GMT"));
    }
    return df.format(d);
  }
  private static final DateFormat dateF =
    DateFormat.getDateInstance(DateFormat.FULL);
  static String dt2uitime(DateTime dt) {
    String str = timeF.format(new Date(dt.getValue()));
    // tweak: remove space before pm, and remove :00 if present.
    // also lowercase
    return str.replaceFirst("(:00)? (?=[AaPp])", "").toLowerCase();
  }
  private static final DateFormat timeF =
    DateFormat.getTimeInstance(DateFormat.SHORT);

  private static final DateFormat dtidF =
    new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'");
  static { dtidF.setTimeZone(TimeZone.getTimeZone("GMT")); }

  static String entry2id(WhenEvent we) {
    // trim off the prefix parts of the entry id.
    String id = we.event.getId().replaceFirst("^.*/","");
    // add in start time to disambiguate recurring events.
    if (!id.contains("_"))
      id += "_" + dtidF.format(new Date(we.when.getStartTime().getValue()));
    // ids must start with a letter.
    id = "x" + id;
    // okay, done.
    return id;
  }
  static String tc2xhtml(TextConstruct tc) {
    if (tc.getType() == TextConstruct.Type.XHTML)
      return ((XhtmlTextConstruct)tc).getXhtml().getBlob();
    // xxx: TextConstruct.Type.HTML needs conversion to XHTML
    // fall back to unformatted.
    String pt = tc.getPlainText();
    // special case when we want to include a hyperlink
    return pt.contains("</a>") ? pt : htmlEscape(pt);
  }
  static String cont2xhtml(Content c) {
    if (c instanceof OtherContent)
      return ((OtherContent)c).getXml().getBlob();
    //return tc2xhtml(((TextContent)c).getContent());
    // XXX this seems to be a bug in Google's API: plain text content is
    // really HTML, so don't unescape it.
    return ((TextContent)c).getContent().getPlainText(); // which isn't actually plain text
  }
  public static String htmlEscape(String s) {
    StringBuilder sb = new StringBuilder();
    for (int i=0; i<s.length(); i++) {
      char c = s.charAt(i);
      if ((c>='0' && c<='9') || (c>='a' && c<='z') || (c==' ') ||
	  (c>='A' && c<='Z') || (c=='.') || (c=='/') || (c=='_'))
	sb.append(c);
      else if (c=='"')
	sb.append("&quot;"); // see XML 1.0 spec sec 2.4
      else if (c=='\'')
	sb.append("&apos;"); // see XML 1.0 spec sec 2.4
      else {
	sb.append("&#");
	sb.append((int)c);
	sb.append(';');
      }
    }
    return sb.toString();
  }

  static void dump(EventFeed myFeed) {
    // Print the title of the returned feed:
    System.out.println(myFeed.getTitle().getPlainText());
    System.out.println(myFeed.getSubtitle().getPlainText());
    // entries
    dump(filter(myFeed.getEntries()));
  }
  static void dump(List<WhenEvent> l) {
    for (WhenEvent we : l) {
      System.out.println("------");
      System.out.println(we.event.getId());
      System.out.println(we.event.getTitle().getPlainText());
      System.out.println(we.event.getStatus().getValue());
      System.out.println(we.event.getTransparency().getValue());
      if (we.event.getVisibility()!=null)
	System.out.println(we.event.getVisibility().getValue());
      for (Where where : we.event.getLocations())
	System.out.println("WHERE: "+where.getValueString());
      System.out.println("WHEN: "+dt2str(we.when.getStartTime())+" to "+dt2str(we.when.getEndTime()));
    }
  }
  static String dt2str(DateTime dt) {
    return dateTimeF.format(new Date(dt.getValue()));
  }
  private static final DateFormat dateTimeF = DateFormat.getDateTimeInstance();
    
  static Calendar dt2cal(DateTime dt) {
    Calendar c = Calendar.getInstance();
    c.setTimeInMillis(dt.getValue());
    return c;
  }

  /** Take an unsorted list of event entries, with recurrences and cancelled
   *  events, and filter out the cancelled events and the "original events"
   *  of recurrences.  Return a list sorted by start time. */
  static List<WhenEvent> filter(List<EventEntry> lee) {
    // create a list of non-cancelled when events (one time per event)
    List<WhenEvent> result = new ArrayList<WhenEvent>();
    for (EventEntry ee : lee)
      for (When when : ee.getTimes())
	result.add(new WhenEvent(when, ee));

    // collect a set of 'original events'
    Set<URITime> orig = new HashSet<URITime>(result.size());
    for (WhenEvent we : result) {
      OriginalEvent oe = we.event.getOriginalEvent();
      if (oe==null) continue;
      orig.add(new URITime(oe.getHref(), we.when.getStartTime()));
    }
    // remove these original events from the result list.
    for (Iterator<WhenEvent> it = result.iterator(); it.hasNext(); ) {
      WhenEvent we = it.next();
      if (orig.contains(new URITime(we.event.getId(), we.when.getStartTime())))
	it.remove();
    }
    // NOW remove cancellations (otherwise we might leave some cancelled
    // original events in the list)
    for (Iterator<WhenEvent> it = result.iterator(); it.hasNext(); )
      if (EventEntry.EventStatus.CANCELED.equals(it.next().event.getStatus()))
	it.remove();
    // heck, remove drafts too
    for (Iterator<WhenEvent> it = result.iterator(); it.hasNext(); )
      if (it.next().event.isDraft())
	it.remove();

    // sort the final list & return it
    Collections.sort(result);
    return Collections.unmodifiableList(result);
  }

  private static class URITime {
    public final String uri;
    public final DateTime start;
    URITime(String uri, DateTime start) {
      this.uri = uri; this.start = start;
    }
    public int hashCode() {
      return uri.hashCode() + 7*start.hashCode();
    }
    public boolean equals(Object o) {
      if (!(o instanceof URITime)) return false;
      URITime ut = (URITime) o;
      return this.uri.equals(ut.uri) && this.start.equals(ut.start);
    }
    public String toString() { return uri+" "+start; }
  }
  static class WhenEvent implements Comparable<WhenEvent> {
    public final When when;
    public final EventEntry event;
    public WhenEvent(When when, EventEntry event) {
      this.when = when; this.event = event;
    }
    public int compareTo(WhenEvent we) {
      DateTime a = this.when.getStartTime();
      DateTime b = we.when.getStartTime();
      int c = a.compareTo(b);
      if (c!=0) return c;
      a = this.when.getEndTime();
      b = we.when.getEndTime();
      if (a==null || b==null)
	return (a!=null) ? -1 : (b!=null) ? 1 : 0;
      return a.compareTo(b);
    }
  }
}
