#!/usr/bin/python3

"""Send automated mail to the Student Activities Office each month so
that Tech Squares can get the details of monthly employee payroll.
Requires an --actually-send argument to actually send to avoid people
running the script and inadvertently sending mail.
"""

import sys
import smtplib

def run():
    """Actually send the mail."""

    msg = """From: tech-squares-treasurer@mit.edu
To: rdownes@mit.edu
Cc: tech-squares-treasurer@mit.edu
Subject: eDACCA Report for Tech Squares

Hi,

Would you please send us the eDACCA report for the previous month for
Tech Squares, cost object 2720265?  (This is an automated mail sent
monthly so that we can confirm that everyone we expected to get paid
via payroll last month successfully got paid.)

Thanks!
"""

    with smtplib.SMTP("outgoing.mit.edu") as smtp:
        smtp.sendmail("tech-squares-treasurer@mit.edu",
                      ["rdownes@mit.edu",
                       "tech-squares-treasurer@mit.edu"],
                      msg)

if __name__ == "__main__":
    if len(sys.argv) == 2 and sys.argv[1] == "--actually-send":
        run()
    else:
        print("No mail actually sent.")
