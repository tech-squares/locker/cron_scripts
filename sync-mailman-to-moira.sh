#!/bin/bash

set -euf -o pipefail

kinit -k -t /mit/tech-squares/Scripts/secrets/tech-squares.keytab daemon/tech-squares.mit.edu
(
  athrun consult mmblanche tech-squares | grep -v -Fx 7of9@mit.edu | sed -e 's/@mit.edu$//' | blanche tech-squares-mailman-sync -f /dev/stdin
) 2>&1 | grep -v "Retrieving membership list.* done"
